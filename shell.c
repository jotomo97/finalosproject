#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>     
#include <sys/wait.h> 
char *buffer;
size_t size = 30; // used to represent the size of an object in bytes (for getline funtion)
char *token;
char *array[500]; 

int main(){
//Display Intro Prompt (Only Appears Onces)
	printf("\n");
	printf(" WELCOME TO OS WIZARDS SHELL         \n");
	printf(" TO EXIT TYPE 'quit'                 \n");
	printf("\n");
// Allows the loop to run inifatly till a break statement is given 
	while(1){
		printf("OS WIZARDS SHELL: "); // Display a line Promt (Displayed at the beginngin of each line)
		getline(&buffer, &size, stdin); // Read the user input
// check is input was enterd  
		if(strcmp(buffer,"\n")==0){
			printf("No Command Enterd Please Try Again \n" );
            return 0;
            }
    
		//Lines 28 - 34 inspired by Sanzhar Zholidyarov
		int i = 0;
		token = strtok(buffer, "\n "); // takes the input and the delimiter and changes then into token in this case the expected delmiter is a space 
		while (token != NULL) { 
			array[i++] = token; // Add tokens into the array
			token = strtok(NULL, "\n "); // continue tokening the same string as before 
			}
		array[i] = NULL;

		//Lines 27-33 were referenced from https://github.com/szholdiyarov/command-line-interpreter/blob/master/myshell.c
        

// check if user entered 'quit' to leave shell 	
        if (strcmp(array[0], "quit") == 0) { 
			printf("Exiting Shell\n");
            printf("Thank You for using OS Wizards Shell\n");
			return 0;
			}

	   	pid_t pid = fork(); // Create a new process
        if (pid < 0 ){
            printf ("Error"); //forking child process failed
            return 0;
            }
		else if (pid == 0) { //Parent Process 
			if(execvp(array[0], array) < 0){ // Child Process
			printf("Command Not Found \n"); 
			return 0;
		}else {
			wait(NULL); // Wait for process termination		
			return 0;
			}				
		}
	}
}